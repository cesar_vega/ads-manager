Ads Manager
-----------

An "ad" is a real world advertisement that's been cataloged in this system.  

Currently, this system offers the following:
 
- Connects incoming *caller* to *call taker*.  
- Uses Twilio API to connect purchased Twilio phone numbers (both text and voice
  capable) to real world phones.
- Basic routing logic - If *caller* has previously been assigned a *call taker*, 
  route incoming call (purchased Twilio number) to the assigned call taker's 
  physical phone. If *caller* is new, assign a new *call taker*.
- Logs basic call information - date, time, length

It is written with the Laravel Framework and offers no tests at the moment. 
The bulk of the application's logic lies in `app/http/controllers/*`. View logic 
is in `public/*` and `resources/*`

**This project is currently running on my real estate website's web hosting:** 
[Ads-Manager] (http://ads.realtyrelief.com)

The Laravel Framework force's PHP developers to adhere to:

- Abstraction
- Modularity
- Use of modern object oriented designs
- Model-View-Controller pattern