<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdCallTaker extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ad_call_takers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ad_id');
            $table->integer('call_taker_id');
            $table->softDeletes();
            $table->timestamps();
            
            $table->foreign('ad_id')
            ->references('id')->on('ads')->onDelete('cascade');
            $table->foreign('call_taker_id')
            ->references('id')->on('call_takers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ad_call_takers');
    }
}
