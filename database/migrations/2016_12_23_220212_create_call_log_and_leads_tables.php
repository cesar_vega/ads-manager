<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCallLogAndLeadsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leads', function (Blueprint $table) {
            $table->increments('id');
            $table->string('number');
            $table->integer('assigned_to');
            $table->integer('initial_ad_id');
            $table->timestamps();
            
            $table->foreign('assigned_to')
            ->references('id')->on('call_takers')->onDelete('cascade');
        });
        
        Schema::create('incoming_calls', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('lead_id');
            $table->integer('call_taker_id');
            $table->integer('ad_id');
            $table->timestamps();
            
            $table->foreign('lead_id')
            ->references('id')->on('leads')->onDelete('cascade');
            $table->foreign('call_taker_id')
            ->references('id')->on('call_takers')->onDelete('cascade');
            $table->foreign('ad_id')
            ->references('id')->on('ads')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('incoming_calls');
        Schema::dropIfExists('leads');
    }
}

