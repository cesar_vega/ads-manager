<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CallTaker;

class CallTakerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $takers = CallTaker::all();
        
        return view('call-takers.index', compact('takers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('call-takers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $taker = new CallTaker($request->all());
        $taker->name = $taker->first . ' ' . $taker->last;
        $taker->save();
        
        return redirect()->route('call-taker.show', ['id' => $taker->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $taker = CallTaker::find($id);
        
        return view('call-takers.show', compact('taker'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $taker = CallTaker::find($id);
        
        return view('call-takers.edit', compact('taker'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $taker = CallTaker::find($id);
        $taker->fill($request->all());
        $taker->name = $taker->first . ' ' . $taker->last;
        $taker->save();
        
        return redirect()->route('call-takers.show', compact('id'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        CallTaker::find($id)->delete();
        
        return redirect()->route('call-taker.index');
    }
}
