<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Ad;
use App\Models\CallTaker;
use App\Models\AdCallTaker;
use Twilio\Rest\Client;
use Illuminate\Support\Facades\View;

class AdController extends Controller
{
    /**
     * @var Twilio\Rest\Client 
     */
    protected $twilio;
    
    public function __construct(Client $twilio)
    {
        $this->twilio = $twilio;
        
        View::share('typeCodes', Ad::getTypeCodes());
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ads = Ad::all();
        
        return view('ads.index', compact('ads'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $takers = CallTaker::all();
        
        return view('ads.create', compact('takers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ad = new Ad;
        $ad->type = $request->get('type');
        $ad->description = $request->get('description');        
        $ad->number = $request->get('number');
        $ad->save();
        
        // Fill in the takers
        foreach ($request->get('takers') as $id)
        {
            $taker = new AdCallTaker;
            $taker->ad_id = $ad->id;
            $taker->call_taker_id = $id;
            $taker->save();
        }
        
        return redirect()->route('ad.show', ['id' => $ad->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ad = Ad::find($id);
        
        return view('ads.show', compact('ad'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ad = Ad::find($id);
        $assignedTakers = $ad->callTakers()->get();
        $avaliableTakers = CallTaker::all();
        
        $takers = [];
        foreach ($avaliableTakers as $avaliableTaker)
        {
            $avaliableTaker->assigned = false; 
            
            foreach ($assignedTakers as $assignedTaker)
            {
                if ($assignedTaker->id == $avaliableTaker->id)
                {
                    $avaliableTaker->assigned = true;
                    break;
                }
            }

            
            $takers[] = $avaliableTaker;
        }
        
        return view('ads.edit', compact('ad', 'takers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $ad = Ad::find($id);
        $ad->type = $request->get('type');
        $ad->description = $request->get('description');
        $ad->number = $request->get('number');
        $ad->save();
        
        // Fill in the takers
        $requestTakers = $request->get('takers');
        $currentTakers = $ad->callTakers()->get();
        if (count($requestTakers) !== count($currentTakers))
        {
            // New Takers
            foreach ($requestTakers as $requestTakerId)
            {
                $create = true;
                foreach ($currentTakers as $currentTaker)
                {
                    if ($currentTaker->id == $requestTakerId)
                    {
                        $create = false;
                        break;
                    }
                }

                if ($create)
                {
                    $taker = new AdCallTaker;
                    $taker->ad_id = $ad->id;
                    $taker->call_taker_id = $requestTakerId;
                    $taker->save();
                }
            }

            // Remove Takers
            foreach ($currentTakers as $currentTaker)
            {
                $delete = true;
                foreach ($requestTakers as $requestTakerId)
                {
                    if ($currentTaker->id == $requestTakerId)
                    {
                        $delete = false;
                        break;
                    }
                }

                if ($delete)
                {
                    AdCallTaker::where('ad_id', '=', $ad->id)
                            ->where('call_taker_id', '=', $currentTaker->id)
                            ->delete();
                }
            }
        }
        
        return redirect()->route('ad.show', ['id' => $ad->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
