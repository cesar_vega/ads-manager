<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Twilio\Rest\Client;
use Twilio\Twiml;
use App\Models\IncomingCall;
use App\Models\Lead;
use App\Models\Ad;

class TwilioRestController extends Controller
{
    /**
     * @var Twilio\Rest\Client 
     */
    protected $twilio;
    
    public function __construct(Client $client)
    {
        $this->twilo = $client;
    }
    
    /**
     * Handle all incoming calls
     * 
     * @param Request $request
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function incomingCalls(Request $request)
    {
        $to = $request->get('To');
        $from = $request->get('From');

        // Get Ad
        $ad = Ad::query()->where('number', '=', $to)->first();
        if ($ad === null)
        {
            throw new \Exception('Phone Number not found: \'' . $to . "'");
        }
        
        // Find or create lead
        $lead = Lead::query()->where('number', '=', $from)->first();
        if ($lead === null)
        {   
            $lead = new Lead;
            $lead->assigned_to =  $ad->getNextCallTaker()->id;
            $lead->initial_ad_id = $ad->id;
            $lead->number = $from;
            $lead->save();
        }
        
        // Log
        $incomingCall = new IncomingCall;
        $incomingCall->lead_id = $lead->id;
        $incomingCall->call_taker_id = $lead->assigned_to;
        $incomingCall->ad_id = $ad->id;
        $incomingCall->save();
        
        // Make call to Lead's Contact
        $twiml = new Twiml();
        $twiml->dial($lead->getContact()->number);
        return response($twiml, 201)->header('Content-Type', 'application/xml');
    }
    
    public function incomingTexts(Request $request)
    {
        return response('Future..', 501);
    }
}
