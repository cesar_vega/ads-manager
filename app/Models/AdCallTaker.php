<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdCallTaker extends Model
{
    Use SoftDeletes;
    
    protected $table = 'ad_call_takers';
    
    protected $guarded = [];
    
    protected $dates = ['deleted_at'];
}
