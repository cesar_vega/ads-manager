<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CallTaker extends Model
{
    Use SoftDeletes;
    
    protected $table = 'call_takers';
    
    protected $guarded = [];
    
    protected $dates = ['deleted_at'];
}
