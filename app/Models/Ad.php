<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\CallTaker;

class Ad extends Model
{
    protected $table = 'ads';
    
    /**
     * $codes => $values of Ad Types.
     * 
     * @var array
     */
    protected static $typeCodes = [
        'fb' => 'Facebook',
        'cl' => 'Craigslist',
        'bs' => 'Bandit Sign',
        'ml' => 'Mail',
        'wb' => 'Website',
    ];
    
    /**
     * Return $codes => $values array of accepted Ad Types. 
     * 
     * @return array
     */
    public static function getTypeCodes()
    {
        return static::$typeCodes;
    }
    
    /**
     * Get Ad's call takers.
     * 
     * @return array 
     */
    public function callTakers()
    {
        return $this->getConnection()->table('call_takers')
                ->join('ad_call_takers', 
                        'ad_call_takers.call_taker_id', '=', 'call_takers.id')
                ->join('ads', 
                        'ad_call_takers.ad_id', '=', 'ads.id')
                ->where('ads.id', '=', $this->id)
                ->whereNull('ad_call_takers.deleted_at')
                ->select('call_takers.*');
    }
    
    /**
     * Get the next person in line to take a call.
     * 
     * @return \App\Models\CallTaker
     */
    public function getNextCallTaker()
    {
        $callTakers = $this->callTakers()->orderBy('id')->get();
        $lastLead = $this->getConnection()->table('leads')
                ->where('initial_ad_id', '=', $this->id)
                ->orderBy('created_at', 'desc')
                ->first();
        
        // First lead of an ad
        if (count($lastLead) < 1)
        {
            return $callTakers[0];
        }
       
        // Find next call taker or circle around array.
        foreach ($callTakers as $key => $taker)
        {
            if ($taker->id == $lastLead->assigned_to)
            {
                break;
            }
        }
        $key = (($key + 2) <= count($callTakers)) ? $key + 1 : 0;
        
        return CallTaker::find($callTakers[$key]->id);
    }
}
