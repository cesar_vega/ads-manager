<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\CallTaker;

class Lead extends Model
{
    protected $table = 'leads';
    
    /**
     * Get Lead's contact person
     * 
     * @return \App\Models\CallTaker
     */
    public function getContact()
    {
        return $this->hasOne(CallTaker::class, 'id', 'assigned_to')->first();
    }
}
