<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class IncomingCall extends Model
{
    protected $table = 'incoming_calls';
}
