@extends('layouts.master')

@section('content')
    <h2>{{ $taker->name }}</h3>
    <p>Phone Number: {{ $taker->number }}</p>
    <a href='{{ route('call-taker.edit', ['id' => $taker->id]) }}'>Edit</a>
@stop
