@extends('layouts.master')

@section('content')
<h3>Add a Call Taker</h3>
{!! Form::open([
    'url' => route('call-taker.store'), 
    'method' => 'post'
]) !!}
    <div class="form-group">
    {!! Form::label('first', 'First Name: ') !!}
    {!! Form::text('first') !!}
    </div>
    <div class="form-group">
    {!! Form::label('last', 'Last Name: ') !!}
    {!! Form::text('last') !!}
    </div>
    <div class="form-group">
    {!! Form::label('number', 'Phone Number: ') !!}
    {!! Form::text('number') !!}
    </div>
    {!! Form::submit('Add', ['class' => 'btn btn-primary']) !!}
{!! Form::close() !!}
@stop

