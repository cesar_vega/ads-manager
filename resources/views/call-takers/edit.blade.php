@extends('layouts.master')

@section('content')
    <h2>Edit a Call Taker </h2>
    {!! Form::model($taker, [
        'route' => ['call-taker.update', $taker->id], 
        'method' => 'put'
    ]) !!}
    <div class="form-group">
        {!! Form::label('first', 'First Name:') !!}
        {!! Form::text('first') !!}
    </div>
    <div class="form-group">
        {!! Form::label('last', 'Last Name:') !!}
        {!! Form::text('last') !!}
    </div>
    <div class="form-group">
        {!! Form::label('number', 'Phone Number:') !!}
        {!! Form::text('number') !!}
    </div>
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    {!! Form::close() !!}
    {!! Form::open(['url' => route('call-taker.destroy', $taker->id),
                'method' => 'DELETE']) !!}
    {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) !!}
    {!! Form::close() !!}
@stop
