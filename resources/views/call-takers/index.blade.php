@extends('layouts.master')

@section('content')    
    <div class="row">
        <div class="col-lg-8">
            <table class="table">
                <thead>
                    <th>Name</th>
                    <th>Phone Number</th>
                    <th>
                      <a href="{{ route('call-taker.create') }}" class="btn btn-primary btn-xs">
                        Add
                      </a>
                    </th>
                </thead>
                <tbody>
                    @foreach ($takers as $taker)
                        <tr>
                            <td> {{ $taker->name }} </td>
                            <td> {{ $taker->number }} </td>
                            <td>
                                {!! Html::link(route('call-taker.edit', $taker->id), 'Edit',
                                               ['class' => 'btn btn-default btn-xs']) !!}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('scripts')
    {!! Html::script('//cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.min.js') !!}
    {!! Html::script('//cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js')!!}
    <!-- {!! Html::script('/js/pieCharts.js') !!} -->
@stop
