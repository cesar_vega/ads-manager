@extends('layouts.master')

@section('content')
    <p>Type: {{ $typeCodes[$ad->type] }}</p>
    <p>Description: {{ $ad->description }}</p>
    <p>Phone Number: {{ $ad->number }}</p>
    <p>Assigned To:</p>
    <ul>
    @foreach ($ad->callTakers()->get() as $taker)
    <li>{{ $taker->name }}</li>
    @endforeach
    </ul>
    
    <a href='{{ route('ad.edit', ['id' => $ad->id]) }}'>Edit</a>
@stop
