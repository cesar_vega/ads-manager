@extends('layouts.master')

@section('content')
<h3>Edit an Ad</h3>
{!! Form::model($ad, [
    'route' => ['ad.update', $ad->id], 
    'method' => 'put',
    'files' => true
]) !!}
    <div class="form-group">
    {!! Form::label('type', 'Type: ') !!}
    {!! Form::select('type',$typeCodes) !!}
    </div>
    <div class="form-group">
    {!! Form::label('description', 'Description: ') !!}
    {!! Form::text('description') !!}
    </div>
    <div class="form-group">
    {!! Form::label('number', 'Phone Number: ') !!}
    {!! Form::text('number') !!}
    </div>
    <h4>Call Takers</h4>      
    @foreach ($takers as $taker)
        <div class="form-group">
        {!! Form::label('takers['.$taker->id.']', $taker->name . ':') !!}
        {!! Form::checkbox(
            'takers['.$taker->id.']', 
            $taker->id,
            $taker->assigned
        ) !!}
        </div>
    @endforeach
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
{!! Form::close() !!}
@stop
