@extends('layouts.master')

@section('content')
<h3>Add an Ad</h3>
{!! Form::open([
    'url' => route('ad.store'), 
    'method' => 'post',
    'files' => true
]) !!}
    <div class="form-group">
    {!! Form::label('type', 'Type: ') !!}
    {!! Form::select('type',$typeCodes) !!}
    </div>
    <div class="form-group">
    {!! Form::label('description', 'Description: ') !!}
    {!! Form::text('description') !!}
    </div>
    <div class="form-group">
    {!! Form::label('number', 'Phone Number: ') !!}
    {!! Form::text('number') !!}
    </div>
    <h4>Call Takers</h4>      
    @foreach ($takers as $taker)
        <div class="form-group">
        {!! Form::label('takers['.$taker->id.']', $taker->name . ':') !!}
        {!! Form::checkbox('takers['.$taker->id.']', $taker->id) !!}
        </div>
    @endforeach
    {!! Form::submit('Add', ['class' => 'btn btn-primary']) !!}
{!! Form::close() !!}
@stop

