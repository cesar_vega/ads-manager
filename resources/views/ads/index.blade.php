@extends('layouts.master')

@section('content')    
    <div class="row">
        <div class="col-lg-8">
            <table class="table">
                <thead>
                    <th>Type</th>
                    <th>Phone Number</th>
                    <th>Description</th>
                    <th>Assigned to</th>
                    <th>
                      <a href="{{ route('ad.create') }}" class="btn btn-primary btn-xs">
                        Add
                      </a>
                    </th>
                </thead>
                <tbody>
                    @foreach ($ads as $ad)
                        <tr>
                            <td> {{ $typeCodes[$ad->type] }} </td>
                            <td> {{ $ad->number }} </td>
                            <td> {{ $ad->description }} </td>
                            <td> 
                              @foreach ($ad->callTakers()->get() as $taker)
                              <div class="btn-xs"> {{ $taker->name }} </div>
                              @endforeach
                            </td>
                            <td>
                                {!! Html::link(route('ad.edit', $ad->id), 'Edit',
                                               ['class' => 'btn btn-default btn-xs']) !!}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('scripts')
    {!! Html::script('//cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.min.js') !!}
    {!! Html::script('//cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js')!!}
    <!-- {!! Html::script('/js/pieCharts.js') !!} -->
@stop
