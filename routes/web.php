<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('ad.index');
});


Route::get('/dashboard', function () {
    return view('dashboard.index');
})->name('dashboard');

Route::resource('ad', 'AdController');
Route::resource('call-taker', 'CallTakerController');

Route::post('/api/v2016/calls/incoming', 'TwilioRestController@incomingCalls');
Route::get('/api/v2016/texts/incoming', 'TwilioRestController@incomingTexts');

Auth::routes();
Route::get('/home', 'HomeController@index');
